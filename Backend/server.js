const express = require("express");
const app = express();
const indexRouter = require("./app/routes/index")
const cors = require('cors')
app.use(express.json());

app.use(express.urlencoded({ extended: true }));
app.use(cors())
const dotenv = require("dotenv");
dotenv.config();

app.get('/', (req, res, next)=>{
  res.send('App is running and use "/api" to use other routes')
});

//Route Prefixes
app.use("/", indexRouter);

// throw 404 if URL not found
app.all("*", function(req, res) {
	res.send("Page not found");
});

const PORT = process.env.PORT || 8080
app.listen(PORT, () => {
  console.log(`Server is running on  http://localhost:${PORT}.`);
})