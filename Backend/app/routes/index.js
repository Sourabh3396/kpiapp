const express = require("express");
const kpiRouter = require ("./kpi.router")

const app = express();

app.use("/api", kpiRouter);

module.exports = app;