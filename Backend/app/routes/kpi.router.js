const router = require("express").Router();
const kpiController = require("../controllers/kpi.controller.js");


//Department
router.post("/Department",kpiController.addDepartment)
router.get("/Department",kpiController.getAllDepartment)

//KPI
router.post("/Kpi",kpiController.addKPI)
router.get("/Kpi",kpiController.getAllKpi)
router.put("/Kpi",kpiController.UpdateKpi)
router.delete('/Kpi',kpiController.deleteKPI)

//KPI Matrix
router.delete('/KpiMatrix',kpiController.deleteKPIMatrix)

//KPI Sub Matrix
router.delete('/KpiSubMatrix',kpiController.deleteSubKPIMatrix)


module.exports = router;