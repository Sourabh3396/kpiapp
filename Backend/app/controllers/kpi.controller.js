//Models
const db = require('../model');
const Department = db.department
const Kpi = db.kpi
const KpiMatrix = db.kpimatrix
const KpiSubMatrix = db.kpisubmatrix

//Add Department
const addDepartment = async (req, res) => {
    await Department.create(req.body).then(function (result) {
        if (result)
            res.status(200).send({ message: "Department Added..", status: 1, data: result });
        else
            res.status(400).send({ message: "Error Inserting Data..", status: 0, data: null })
    }).catch(function (err) {
        res.status(400).send({ message: err, status: 0, data: null })
    });

}

//Get All Department 
const getAllDepartment = async (req, res) => {
    await Department.findAll().then(function (result) {
        if (result)
            res.status(200).send({ message: "Department Retrived..", status: 1, data: result });
        else
            res.status(400).send({ message: "Error Retriving Data..", status: 0, data: null })
    }).catch(function (err) {
        res.status(400).send({ message: err, status: 0, data: null })
    });

}


//Add KPI
const addKPI = async (req, res) => {
    const transaction = await db.sequelize.transaction();
    let content = req.body;
    let Kpicnt = { name: content.name, description: content.description, departmentId: content.departmentName, has_self_Review: content.has_self_Review == true ? 1 : 0 }
    await Kpi.create(Kpicnt, { transaction: transaction }).then(async function (resKpi) {
        if (resKpi) {
            for (let i = 0; i < content.KpiMatrix.length; i++) {
                let Kpimatrixcnt = {
                    title: content.KpiMatrix[i].title, tooltip: content.KpiMatrix[i].tooltip, weightage: content.KpiMatrix[i].weightage, kpiId: resKpi.id
                }
                await KpiMatrix.create(Kpimatrixcnt, { transaction: transaction }).then(async function (resKpimatrix) {
                    if (resKpimatrix) {
                        let KpiSubMatrixArr = []
                        for (j = 0; j < content.KpiMatrix[i].kpiSubMatrix.length; j++) {
                            let Kpisubmatrixcnt = {
                                name: content.KpiMatrix[i].kpiSubMatrix[j].name, kpiMatrixId: resKpimatrix.id
                            }
                            KpiSubMatrixArr.push(Kpisubmatrixcnt)
                        };
                        await KpiSubMatrix.bulkCreate(KpiSubMatrixArr, { transaction: transaction }).then(async function (resKpisubmatrix) {
                            if (resKpisubmatrix) {
                            } else {
                                await transaction.rollback()
                                res.status(400).send({ message: "Error Adding Data..", status: 0, data: null })
                            }
                        }).catch(async function (err) {
                            await transaction.rollback()
                            res.status(400).send({ message: err, status: 0, data: null })
                        });

                    } else {
                        await transaction.rollback()
                        res.status(400).send({ message: "Error Adding Data..", status: 0, data: null })
                    }
                }).catch(async function (err) {
                    await transaction.rollback()
                    res.status(400).send({ message: err, status: 0, data: null })
                });
            }
            await transaction.commit()
            res.status(200).send({ message: "Kpi Added Successfully..", status: 1, data: null })

        } else {
            await transaction.rollback()
            res.status(400).send({ message: "Error Adding Data..", status: 0, data: null })
        }
    }).catch(async function (err) {
        await transaction.rollback()
        res.status(400).send({ message: err, status: 0, data: null })
    });

}

//Get All KPI
const getAllKpi = async (req, res) => {
    await Kpi.findAll({
        include: [
            { model: Department },
            {
                model: KpiMatrix,
                include: [{
                    model: KpiSubMatrix
                }]
            }
        ]
    }).then(function (result) {
        if (result)
            res.status(200).send({ message: "Success", status: 1, data: result });
        else
            res.status(400).send({ message: "Error Retriving Data..", status: 0, data: null })
    }).catch(function (err) {
        res.status(400).send({ message: err, status: 0, data: null })
    });

}

//Update Individual KPI
const UpdateKpi = async (req, res) => {
    var content = req.body;
    await Kpi.findAll({
        where: { id: content.id },
        include: [
            { model: Department },
            {
                model: KpiMatrix,
                include: [{
                    model: KpiSubMatrix
                }]
            }
        ]
    }
    ).then(async function (result) {
        if (result) {
            const transaction = await db.sequelize.transaction();
            var recentCreatedKpi
            let kpiUdpate = { name: content.name, description: content.description, departmentId: content.departmentName, has_self_Review: content.has_self_Review == true ? 1 : 0 }
            await Kpi.update(kpiUdpate, { where: { id: result[0].id } }).then(async function (kpires) {
                if (kpires) {
                    for (var i = 0; i < content.KpiMatrix.length; i++) {
                        if (content.KpiMatrix[i].id) {
                            await KpiMatrix.update(content.KpiMatrix[i], { where: { id: content.KpiMatrix[i].id } }).then(async function (kpimatrixres) {
                                if (kpimatrixres) { }
                                else {
                                    res.status(400).send({ message: "Error Updating Data..", status: 0, data: null })
                                    await transaction.rollback()
                                }
                            }).catch(async function (err) {
                                res.status(400).send({ message: err, status: 0, data: null })
                                await transaction.rollback()
                            });
                        }
                        else {
                            await KpiMatrix.create(content.KpiMatrix[i]).then(async function (kpimatrixres) {
                                if (kpimatrixres) { recentCreatedKpi = kpimatrixres }
                                else {
                                    res.status(400).send({ message: "Error Updating Data..", status: 0, data: null })
                                    await transaction.rollback()
                                }
                            }).catch(async function (err) {
                                res.status(400).send({ message: err, status: 0, data: null });
                                await transaction.rollback()
                            });
                        }
                        for (var j = 0; j < content.KpiMatrix[i].kpiSubMatrix.length; j++) {
                            if (content.KpiMatrix[i].kpiSubMatrix[j].id) {
                                await KpiSubMatrix.update(content.KpiMatrix[i].kpiSubMatrix[j], { where: { id: content.KpiMatrix[i].kpiSubMatrix[j].id } }).then(async function (kpisubmatrixres) {
                                    if (kpisubmatrixres) { }
                                    else {
                                        res.status(400).send({ message: "Error Updating Data..", status: 0, data: null })
                                        await transaction.rollback()
                                    }
                                }).catch(async function (err) {
                                    res.status(400).send({ message: err, status: 0, data: null })
                                    await transaction.rollback()
                                });
                            }
                            else {
                                let Kpisubmatrixcnt = {
                                    name: content.KpiMatrix[i].kpiSubMatrix[j].name, kpiMatrixId: content.KpiMatrix[i].kpiSubMatrix[j].kpiMatrixId ? content.KpiMatrix[i].kpiSubMatrix[j].kpiMatrixId : recentCreatedKpi.id
                                }

                                await KpiSubMatrix.create(Kpisubmatrixcnt).then(async function (kpisubmatrixres) {
                                    if (kpisubmatrixres) { }
                                    else {
                                        res.status(400).send({ message: "Error Updating Data..", status: 0, data: null })
                                        await transaction.rollback()
                                    }
                                }).catch(async function (err) {
                                    res.status(400).send({ message: err, status: 0, data: null })
                                    await transaction.rollback()
                                });
                            }
                        }
                    }

                    res.status(200).send({ message: "Kpi Updated Successfully..", status: 1, data: null });
                    await transaction.commit()
                }
                else {
                    res.status(400).send({ message: "Error Updating Data..", status: 0, data: null })
                    await transaction.rollback()
                }
            }).catch(async function (err) {
                res.status(400).send({ message: err, status: 0, data: null })
                await transaction.rollback()
            });
        }
        else {
            res.status(400).send({ message: "Error Updating Data..", status: 0, data: null })
            await transaction.rollback()
        }
    }).catch(async function (err) {
        res.status(400).send({ message: err, status: 0, data: null })
        await transaction.rollback()
    });

}

//Delete KPIMatrix
const deleteKPIMatrix = async (req, res) => {
    let kpimatrixid = req.query.id;
    await KpiMatrix.destroy({ where: { id: kpimatrixid } }).then(function (result) {
        if (result)
            res.status(200).send({ message: "KPI Matrix Deleted..", status: 1, data: null });
        else
            res.status(400).send({ message: "Error Deleting Data..", status: 0, data: null })
    }).catch(function (err) {
        res.status(400).send({ message: err, status: 0, data: null })
    });

}


//Delete SubKPIMatrix
const deleteSubKPIMatrix = async (req, res) => {
    let subkpimatrixid = req.query.id;
    await KpiSubMatrix.destroy({ where: { id: subkpimatrixid } }).then(function (result) {
        if (result)
            res.status(200).send({ message: "KPI Sub Matrix Deleted..", status: 1, data: null });
        else
            res.status(400).send({ message: "Error Deleting Data..", status: 0, data: null })
    }).catch(function (err) {
        res.status(400).send({ message: err, status: 0, data: null })
    });

}


//Delete SubKPIMatrix
const deleteKPI = async (req, res) => {
    let kpiId = req.query.id;
    await Kpi.destroy({ where: { id: kpiId } }).then(function (result) {
        if (result)
            res.status(200).send({ message: "KPI Deleted..", status: 1, data: null });
        else
            res.status(400).send({ message: "Error Deleting Data..", status: 0, data: null })
    }).catch(function (err) {
        res.status(400).send({ message: err, status: 0, data: null })
    });

}



module.exports = {
    addDepartment,
    getAllDepartment,
    addKPI,
    getAllKpi,
    UpdateKpi,
    deleteKPIMatrix,
    deleteSubKPIMatrix,
    deleteKPI


}

