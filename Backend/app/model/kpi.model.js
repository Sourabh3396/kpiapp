module.exports = (sequelize, DataTypes) => {

    const Kpi = sequelize.define("kpi", {
        name: {
            type: DataTypes.STRING(50),
            allowNull: false,
            comment: 'KPI Template Name'
        },
        description: {
            type: DataTypes.STRING(100),
            allowNull: false,
            comment: 'KPI Description'
        },
        has_self_Review: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            comment: 'Self Review 0 means disable 1 means enable'
        }
    })
    return Kpi
}
