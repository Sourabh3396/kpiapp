module.exports = (sequelize, DataTypes) => {

    const KpiMatrix = sequelize.define("kpi_matrix", {
        title: {
            type: DataTypes.STRING(50),
            allowNull: false,
            comment: 'KPI Matrix Title'
        },
        tooltip: {
            type: DataTypes.STRING(100),
            allowNull: false,
            comment: 'KPI Matrix Tooltip'
        },
        weightage: {
            type: DataTypes.STRING(10),
            allowNull: false,
            comment: 'KPI Matrix Weightage'
        }
    })
    return KpiMatrix
}
