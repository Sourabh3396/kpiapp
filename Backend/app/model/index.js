const dbConfig = require('../config/db.config.js');

const {Sequelize, DataTypes} = require('sequelize');
const sequelize = new Sequelize(
    dbConfig.DB,
    dbConfig.USER,
    dbConfig.PASSWORD, {
        host: dbConfig.HOST,
        dialect: dbConfig.dialect,
        dialectOptions: {
            dateStrings: true,
            typeCast: true,
            timezone: "+05:30"
          },
          timezone: "+05:30",
        operatorsAliases: false,

        pool: {
            max: dbConfig.pool.max,
            min: dbConfig.pool.min,
            acquire: dbConfig.pool.acquire,
            idle: dbConfig.pool.idle
        }
    }
)

sequelize.authenticate()
.then(() => {
    console.log('connected..')
})
.catch(err => {
    console.log('Error'+ err)
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.kpi = require('./kpi.model.js')(sequelize, DataTypes);
db.department = require('./department.model.js')(sequelize, DataTypes); 
db.kpimatrix = require('./kpimatrix.model.js')(sequelize, DataTypes); 
db.kpisubmatrix = require('./kpisubmatrix.model.js')(sequelize, DataTypes); 

db.sequelize.sync({ force: false })
.then(() => {
    console.log('yes re-sync done!')
})


db.department.hasMany(db.kpi);
db.kpi.belongsTo(db.department);


db.kpi.hasMany(db.kpimatrix, { onDelete: 'cascade' });
db.kpimatrix.belongsTo(db.kpi);

db.kpimatrix.hasMany(db.kpisubmatrix, { onDelete: 'cascade' });
db.kpisubmatrix.belongsTo(db.kpimatrix);


module.exports = db
