module.exports = (sequelize, DataTypes) => {

    const KpiSubMatrix = sequelize.define("kpi_sub_matrix", {
        name: {
            type: DataTypes.STRING(50),
            allowNull: false,
            comment: 'KPI Sub Matrix Name'
        }
    })
    return KpiSubMatrix
}
