
import { Component, OnInit} from '@angular/core';
import {Router } from '@angular/router';
import { CommonService } from '../services/common.service';
import { ApiCallService } from '../services/api-call.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  loginForm: FormGroup;
  constructor(public fb: FormBuilder, private router: Router, private commanService: CommonService, private apiService: ApiCallService) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    })
  }
  ngOnInit() {
    document.body.style.backgroundColor = "#E5E7E9";
    this.loginForm.patchValue({
      email:'admin@codegine',
      password:'Admin@123'
    })
  }

  ngOnDestroy() {
    document.body.style.background = '#fff';
  }
  submitForm() 
  {
  
    if (this.loginForm.valid) 
    {
     if(this.loginForm.value.email =='admin@codegine' && this.loginForm.value.password =='Admin@123')
     {
      this.commanService.showSuccess('Login Successful..');
      sessionStorage.setItem('user',JSON.stringify(this.loginForm.value));
      this.router.navigate(['components']);
     }
     else
     {
      this.commanService.showError("Invalid Credentails..")
     }
    }
  }

}
