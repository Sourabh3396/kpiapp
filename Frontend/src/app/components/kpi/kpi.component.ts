import { Component, OnInit, ViewChild } from "@angular/core";
import {FormBuilder,FormGroup,FormArray,FormGroupDirective,Validators,} from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { ApiCallService } from "src/app/services/api-call.service";
import { CommonService } from "src/app/services/common.service";
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: "app-kpi",
  templateUrl: "./kpi.component.html",
  styleUrls: ["./kpi.component.scss"],
})
export class KpiComponent implements OnInit {
  displayedColumns: string[] = ["Department", "Name", "Description", "Action"];
  isActiveTab: number = 0;
  isSubmitted: boolean = false;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(FormGroupDirective) myForm: any;
  kpiForm: FormGroup;
  DepartmentList: any;
  tabTitle: any = "Add";
  buttonTitle: any = "Save KPI";
  KpiList: any;
  constructor(
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private apiCallService: ApiCallService,
    private commonService: CommonService
  ) {
    this.kpiForm = this.fb.group({
      id: [null],
      name: [null, [Validators.required, Validators.maxLength(50)]],
      description: [null, [Validators.required, Validators.maxLength(100)]],
      departmentName: [null, [Validators.required]],
      has_self_Review: [false, [Validators.required]],
      KpiMatrix: this.fb.array([this.KpiMatrixdataGroup()]),
    });
  }

  async ngOnInit() {
    //set background color for the page
    document.body.style.backgroundColor = "#E5E7E9";
    //Get All Departments
    this.DepartmentList = await this.getDepartments();
    //Get All KPI's and Display Table
    var data: any = await this.getAllKPI();
    this.KpiList = new MatTableDataSource(data);
    this.KpiList!.paginator = this.paginator;
    this.KpiList!.sort = this.sort;
  }

  //KpiMatrix Form Array function
  private KpiMatrixdataGroup(): FormGroup {
    return this.fb.group({
      id: [null],
      title: [null, [Validators.required, Validators.maxLength(50)]],
      tooltip: [null, [Validators.required, Validators.maxLength(100)]],
      weightage: [null, [Validators.required, Validators.maxLength(10)]],
      kpiId: [null],
      kpiSubMatrix: new FormArray([this.KpiSubMatrixGroup()]),
    });
  }

  //SubMatrix Form Array Function
  private KpiSubMatrixGroup(): FormGroup {
    return this.fb.group({
      id: [null],
      name: [null, [Validators.required, Validators.maxLength(50)]],
      kpiMatrixId: [null],
    });
  }

  get KpiMatrixArrayError(): any {
    return (<FormArray>this.kpiForm.get("KpiMatrix")) as FormArray;
  }

  public myErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.KpiMatrixArrayError["controls"][i]["controls"][
      controlName
    ].hasError(errorName);
  };

  KpiMatrix(): any {
    return (<FormArray>this.kpiForm.get("KpiMatrix")) as FormArray;
  }

  //add KpiMatrix Form Array
  addKpiMatrix() {
    this.KpiMatrix().push(this.KpiMatrixdataGroup());
    if (this.buttonTitle == "Update KPI") {
      (<FormGroup>this.KpiMatrix().at(this.KpiMatrix().length - 1)).patchValue({
        kpiId: this.KpiMatrix().value[0].kpiId,
      });
    }
  }

  //remove KpiMatrix Form Array
  async removeKpiMatrix(index: number) {
    if (this.buttonTitle == "Update KPI") {
      console.log("kpi detal", this.KpiMatrix().value[index].id);
      await this.deleteKPIMatrix(this.KpiMatrix().value[index].id);
      this.KpiMatrix().removeAt(index);
    } else {
      this.KpiMatrix().removeAt(index);
    }
  }

  get kpiSubMatrixArrayError(): any {
    return (<FormArray>this.kpiForm.get("kpiSubMatrix")) as FormArray;
  }

  KpiSubMatrix(index: number): any {
    return (<FormArray>(
      this.KpiMatrix().at(index).get("kpiSubMatrix")
    )) as FormArray;
  }

  //add SubMatrix Form Array
  addKpiSubMatrix(index: number) {
    this.KpiSubMatrix(index).push(this.KpiSubMatrixGroup());
    if (this.buttonTitle == "Update KPI") {
      (<FormGroup>(
        this.KpiSubMatrix(index).at(this.KpiSubMatrix(index).length - 1)
      )).patchValue({
        kpiMatrixId: this.KpiSubMatrix(index).value[0].kpiMatrixId,
      });
    }
  }

  //remove Kpi SubMatrix Form Array
  async removeKpiSubMatrix(i: number, j: number) {
    if (this.buttonTitle == "Update KPI") {
      console.log("kpi detal", this.KpiSubMatrix(i).value[j].id);
      await this.deleteKPISubMatrix(this.KpiSubMatrix(i).value[j].id);
      this.KpiSubMatrix(i).removeAt(j);
    } else {
      this.KpiSubMatrix(i).removeAt(j);
    }
  }

  public myError = (controlName: string, errorName: string) => {
    return this.kpiForm.controls[controlName].hasError(errorName);
  };

  async getDepartments() {
    this.spinner.show();
    return new Promise((resolve, reject) => {
      let data = { data: null, head: null };
      this.apiCallService.getDepartments(data).subscribe({
        next: async (result: any) => {
          if (result) {
            if (result.status) {
              resolve(result.data);
              this.spinner.hide();
            } else {
              reject(false);
              this.spinner.hide();
            }
          }
        },
        error: (error: any) => {
          this.commonService.showError(error);
          reject(false);
          this.spinner.hide();
        },
      });
    });
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.KpiList!.filter = filterValue.trim().toLowerCase();
    if (this.KpiList!.paginator) {
      this.KpiList!.paginator.firstPage();
    }
  }

  tabIsActive(value: any, change: any) {
    this.isActiveTab = 0;
    this.buttonTitle = "Add KPI";
    this.tabTitle = "Add";
    this.kpiForm.reset();
    this.myForm.resetForm();
  }

  submitForm() {
    this.isSubmitted = true;
    if (!this.kpiForm.valid) {
      return;
    }
    this.spinner.show();
    let api;
    this.buttonTitle == "Update KPI"
      ? (api = this.apiCallService.updateKpi)
      : (api = this.apiCallService.addKpi);
    var data: any = { data: this.kpiForm.value };
    api(data).subscribe({
      next: async (result: any) => {
        if (result) {
          if (result.status) {
            this.commonService.showSuccess(result.message);
            this.spinner.hide();
            var data: any = await this.getAllKPI();
            this.KpiList = new MatTableDataSource(data);
            this.KpiList!.paginator = this.paginator;
            this.KpiList!.sort = this.sort;
            this.isActiveTab++;
            this.kpiForm.reset();
            this.myForm.resetForm();
            this.KpiMatrix().clear();
            this.addKpiMatrix();
          } else {
            this.spinner.hide();
            this.commonService.showSuccess("Error while Adding");
          }
        }
      },
      error: (error: any) => {
        this.commonService.showError(error);
        this.spinner.hide();
      },
    });
  }

  getAllKPI() {
    this.spinner.show();
    return new Promise((resolve, reject) => {
      let data = { data: null, head: null };
      this.apiCallService.getKpi(data).subscribe({
        next: async (result: any) => {
          if (result) {
            if (result.status) {
              resolve(result.data);
              this.spinner.hide();
            } else {
              reject(false);
            }
          }
        },
        error: (error: any) => {
          this.commonService.showError(error);
          reject(false);
        },
      });
    });
  }

  updateEvent(data: any) {
    this.kpiForm.patchValue({
      id: data.id,
      name: data.name,
      description: data.description,
      departmentName: data.departmentId,
      has_self_Review: data.has_self_Review,
    });

    if (this.KpiMatrix().length !== 0) {
      this.KpiMatrix().clear();
    }

    data.kpi_matrices.forEach((element: any, index: any) => {
      this.addKpiMatrix();
      (<FormGroup>this.KpiMatrix().at(index)).patchValue({
        id: element.id,
        title: element.title,
        tooltip: element.tooltip,
        weightage: element.weightage,
        kpiId: element.kpiId,
      });
      if (this.KpiSubMatrix(index).length !== 0) {
        this.removeKpiSubMatrix(index, 0);
      }
      element.kpi_sub_matrices.forEach((submatrix: any, indx: any) => {
        this.addKpiSubMatrix(index);
        (<FormGroup>this.KpiSubMatrix(index).at(indx)).patchValue({
          id: submatrix.id,
          kpiMatrixId: submatrix.kpiMatrixId,
          name: submatrix.name,
        });
      });
    });

    this.isActiveTab--;
    this.tabTitle = "Update";
    this.buttonTitle = "Update KPI";
  }

  deleteKPIMatrix(kpimatrixid: any) {
    this.spinner.show();
    return new Promise((resolve, reject) => {
      let data = { data: null, param: { id: kpimatrixid } };
      this.apiCallService.deleteKPIMatrix(data).subscribe({
        next: async (result: any) => {
          if (result) {
            console.log("Result", result);
            if (result.status) {
              resolve(result);
              this.spinner.hide();
              this.commonService.showSuccess(result.message);
            } else {
              reject(false);
            }
          }
        },
        error: (error: any) => {
          this.commonService.showError(error);
          reject(false);
        },
      });
    });
  }

  deleteKPISubMatrix(kpisubmatrixid: any) {
    this.spinner.show();
    return new Promise((resolve, reject) => {
      let data = { data: null, param: { id: kpisubmatrixid } };
      this.apiCallService.deleteKPISubMatrix(data).subscribe({
        next: async (result: any) => {
          if (result) {
            console.log("Result", result);
            if (result.status) {
              resolve(result);
              this.spinner.hide();
              this.commonService.showSuccess(result.message);
            } else {
              reject(false);
            }
          }
        },
        error: (error: any) => {
          this.commonService.showError(error);
          reject(false);
        },
      });
    });
  }

  deleteKPi(kpi: any) {
    this.spinner.show();
    return new Promise((resolve, reject) => {
      let data = { data: null, param: { id: kpi.id } };
      this.apiCallService.deleteKpi(data).subscribe({
        next: async (result: any) => {
          if (result) {
            console.log("Result", result);
            if (result.status) {
              resolve(result);
              this.spinner.hide();
              this.commonService.showSuccess(result.message);
              //Get All KPI's and Display Table
              var data: any = await this.getAllKPI();
              this.KpiList = new MatTableDataSource(data);
              this.KpiList!.paginator = this.paginator;
              this.KpiList!.sort = this.sort;
            } else {
              reject(false);
            }
          }
        },
        error: (error: any) => {
          this.commonService.showError(error);
          reject(false);
        },
      });
    });
  }
}
