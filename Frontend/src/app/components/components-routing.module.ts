import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponentsComponent } from './components.component';
import { KpiComponent } from './kpi/kpi.component';

const routes: Routes = [
  {
    path: '', component: ComponentsComponent, children: [
      {
        path: '',
        redirectTo: 'KPI',
        pathMatch: 'full'
      },
        {
          path:'KPI',
          component:KpiComponent
        },
  
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }
