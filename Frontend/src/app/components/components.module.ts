import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '../../app/Shared/material.module';
import { ApiCallService } from '../services/api-call.service';
import { CommonService } from '../services/common.service';
import { NavbarComponent } from './Shared/navbar/navbar.component';
import { FlexLayoutModule } from "@angular/flex-layout";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ComponentsComponent } from './components.component';
import { ComponentsRoutingModule } from './components-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HttpClientModule } from '@angular/common/http';
import { KpiComponent } from './kpi/kpi.component'; 

@NgModule({
  declarations: [ComponentsComponent, NavbarComponent, KpiComponent],
  imports: [
    HttpClientModule,
    CommonModule,
    ComponentsRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
  ],
  providers:[
    ApiCallService,
    CommonService
  ]
})
export class ComponentsModule { }
