import { Component, ViewChild, HostListener, EventEmitter, OnInit, Output } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { MatSidenav } from '@angular/material/sidenav';
import { ActivatedRoute, Router } from '@angular/router';
import screenfull from 'screenfull';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Output() public sidenavToggle = new EventEmitter();
  isExpanded = true;
  hide = true;
  menus: any;
  screenWidth?: number;
  private screenWidth$ = new BehaviorSubject<number>(window.innerWidth);

  menuList: any = []
  @ViewChild('sidenav') sidenav?: MatSidenav;
  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.screenWidth$.next(event.target.innerWidth);
  }
  constructor(private router: Router) {
    this.menuList = [{
      "text": "KPI",
      "icon": "post_add",
      "routerLink": "/components/KPI"
    },{
      "text": "Logout",
      "icon": "logout",
      "routerLink": "/components/logout"
    }
    ]
  }

  ngOnInit(): void {
    this.screenWidth$.subscribe(width => {
      this.screenWidth = width;
    });
  }

  logout() {
    sessionStorage.clear();
    this.router.navigate(['login']);
  }
  toggleFullscreen() {
    if (screenfull.isEnabled) {
      screenfull.toggle();
      this.hide = !this.hide
    }
  }

}