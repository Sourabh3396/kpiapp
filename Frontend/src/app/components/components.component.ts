import { Component, OnInit } from '@angular/core';
import { ApiCallService } from '../services/api-call.service';
import { CommonService } from '../services/common.service';
import { promise } from 'protractor';
import { reject, result } from 'lodash';
import { resolve } from 'path';
import { error } from 'console';

@Component({
  selector: 'app-components',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.scss']
})
export class ComponentsComponent implements OnInit {

  constructor(private apiCallService: ApiCallService, private commonService: CommonService) { }

  ngOnInit(): void {
  }
 
}
