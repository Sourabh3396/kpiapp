import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiCallService {

  constructor(private http : HttpClient, private common: CommonService) { }
  
  getDepartments = (data : any) => this.common.doGet('Department',data);
  addKpi = (data : any) => this.common.doPost('Kpi',data);
  getKpi = (data : any) => this.common.doGet('Kpi',data);
  updateKpi = (data : any) => this.common.doPut('Kpi',data);
  deleteKpi = (data : any) => this.common.doDelete('Kpi',data);
  deleteKPIMatrix = (data : any) => this.common.doDelete('KpiMatrix',data);
  deleteKPISubMatrix = (data : any) => this.common.doDelete('KpiSubMatrix',data);
}




